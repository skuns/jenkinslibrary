def call(String gitProject) {
    pipeline {
        agent any
        stages {
            stage('Clone') {
                steps {
                    git url: "${gitUrl}"
                }
            }
            stage('Compile') {
                steps {
                    sh "./mvnw package"
                }
            }
        }
    }
}
